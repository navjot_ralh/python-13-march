# # def person(name, age):
# def person(**kwargs):                               # for passing multiple arguments by keyword, use double star(**); for multiple values, use single star(*)
#     # return 'name:{},age:{}'.format(name,age)
#     # return kwargs
#     for i in kwargs:
#         print(kwargs.keys())
#         return '{}={}'.format(i,kwargs[i])

# a=person(name='ABCD', age=25)
# b=person(name='python', age=30, hobby='reading')
# c=person(name='mann', age=30, hobby='reading', roll_no=1, clss='python')
# print(a)
# print(b)
# print(c)

def person(**kwargs):
    for x,y in kwargs.items():
        print('{}={}'.format(x,y))
    print('\n')

person(name='ABCD',age=25)
person(name='python', age=30, hobby='reading')
person(name='mann', age=30, hobby='reading', roll_no=1, clss='python')