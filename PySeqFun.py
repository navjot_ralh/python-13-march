no=[10,200,4,18,2,1]
def mul(n):
    return n*2

# Map Function
m=list(map(mul,no))             # for combining two things; list and function(mul)
print(m)

# using lambda
mp=list(map(lambda x:x%2,no))
print(mp)

# Reduce Function

# Filter Function